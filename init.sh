#!/bin/bash


# Add your application start code from this point onwards


if [[ $API_KEY ]];
then
    sed -i -e "s/^.*api_key:.*$/api_key: ${API_KEY}/" ~/.datadog-agent/agent/datadog.conf
else
    echo "You must set API_KEY environment variable to run the Datadog Agent container"
    exit 1
fi

if [[ $TAGS ]]; then
	sed -i -e "s/^#tags:.*$/tags: ${TAGS}/" ~/.datadog-agent/agent/datadog.conf
fi

if [[ $LOG_LEVEL ]]; then
    sed -i -e"s/^.*log_level:.*$/log_level: ${LOG_LEVEL}/" ~/.datadog-agent/agent/datadog.conf
fi

# The agent needs to be executed with the current working directory set datadog-agent directory
cd /root/.datadog-agent
./bin/agent start &

while :
do
	echo "Running..."
	sleep 60
done
